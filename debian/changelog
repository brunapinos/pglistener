pglistener (4.5) unstable; urgency=medium

  * setup.py: updated version to 4.5.
  * test.py: changed import from bsddb to bsddb3.
  * debian/control: fixed Maintainer name.
  * debian/tests/test.py: updated with fixes for Python3.
  * pglistener/apachevhost.py:
      - changed 'pglistener import' to '.pglistener import'.
  * pglistener/config.py:
      - changed 'ConfigParser' to 'configparser'.
  * pglistener/flatfile.py:
      - changed 'pglistener import' to '.pglistener import'.
      - changed 'has_key' to 'in'.
  * pglistener/mailinglist.py:
      - changed 'pglistener import' to '.pglistener import'.
  * pglistener/nssdb.py:
      - changed 'pglistener import' to '.pglistener import'
      - changed 'import bsddb.db' to 'import bsddb3.db'.
      - changed 'for field in db_keys.keys()' encode.
  * pglistener/nssgroupdb.py:
      - changed 'nssdb import' to '.nssdb import'.
  * pglistener/nsspasswddb.py:
      - changed 'nssdb import' to '.nssdb import'.
  * pglistener/nssshadowdb.py:
      - changed 'nssdb import' to '.nssdb import'.
  * pglistener/pglistener.py:
       - removed 'import err'.
       - changed file 'class' to 'open'.
  * pglistener/simpledbm.py
       - changed 'pglistener import' to '.pglistener import'.
  * pglistener/sshkeys.py:
       - changed 'pglistener import' to '.pglistener import'.

 -- Paulo Henrique de Lima Santana (phls) <phls@debian.org>  Tue, 02 Nov 2021 09:56:16 -0300

pglistener (4.4) unstable; urgency=medium

  * NMU.
  * Updated to python3 on tests.

 -- Paulo Henrique de Lima Santana (phls) <phls@debian.org>  Mon, 27 Sep 2021 10:26:59 -0300

pglistener (4.3) unstable; urgency=medium

  * New upstream version 4.3. (Closes: #966771, #943205, #965771).
  * debian/clean: created.
  * debian/tests/control: updated Depends to python3.
  * On version 4.2 had to fix some python2 sintax on these files below
    to the code works on python3:
      - pglistener/daemon.py
      - pglistener/pglistener.py

 -- Paulo Henrique de Lima Santana (phls) <phls@debian.org>  Thu, 23 Sep 2021 10:13:06 -0300

pglistener (4.2) unstable; urgency=medium

  * New upstream version 4.2.
  * debian/control:
      - Added X-Python3-Version.
      - Updated packages for python3.
      - Bumped Standards-Version to 4.6.0.
      - New format to debhelper-compat and updated level to 13.
  * debian/compat: deleted.
  * debian/rules: updated for python3 --buildsystem=pybuild.

 -- Paulo Henrique de Lima Santana (phls) <phls@debian.org>  Wed, 22 Sep 2021 22:55:59 -0300

pglistener (4.1) unstable; urgency=medium

  * New upstream version 4.1.
  * Release with updates from 2017 to 2021.
  * debian/control:
      - Added 'Rules-Requires-Root: no' in source stanza.
      - Changed maintainer for Paulo Henrique de Lima Santana.
      - Updated extended description.
  * debian/copyright:
      - Changed source repository.
      - Updated software and packaging authors and copyright years.
  * debian/manpage/*: created for pglistener, pglistener-update and pua.
  * debian/manpages: created.

 -- Paulo Henrique de Lima Santana (phls) <phls@debian.org>  Wed, 22 Sep 2021 17:34:13 -0300

pglistener (4+nmu1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Vivek Das Mohapatra ]
  * Treat STDIN as utf8.

  [ Jordi Mallach ]
  * Add Recommends on libterm-readline-gnu-perl.

  [ Maxime “pep” Buquet ]
  * NssDb: Python uses 4 spaces
  * NssDb: Prevent crashes when tmp db already exists
  * Fix python coding style and linting issues
  * mailinglist: Use wrapper func instead of lambda to prevent
    cell-var-from-loop lint error
  * Fix nss db mode (octal not hexa)
  * daemon: use octal for opening mode
  * nssdb: don't shadow db variable
  * update: use dict comprehension
  * Implement sdnotify to help monitoring via systemd
  * sdnotify: declare notifier even when sdnotify isn't available
  * Add systemd service file for sdnotify features
  * debian/control: add python-sdnotify as recommend
  * nssdb: fix variable name
  * setup.py: Use setuptools instead of distutils.
  * Rename scripts and fix python2 shebang

  [ Jordi Mallach ]
  * Add $local_fs to Required-Start and Required-Stop in init script.
  * Use debhelper compat v11.
  * Rewrite copyright in machine-readable format v1.
  * Add Vcs-* fields.
  * Add Homepage field.
  * Update Standards-Version to 4.3.0.
  * Make the source package format 3.0 (native).
  * Drop --parallel from dh invocation, not needed in v11.
  * Use dh-python and generate deps via ${python:Depends}.
  * Move from /var/run to /run.
  * Adapt to renamed scripts location.
  * Drop obsolete X-Python-Versions field.

 -- Jordi Mallach <jordi@debian.org>  Fri, 03 May 2019 16:20:12 +0200

pglistener (4) unstable; urgency=medium

  * Add missing ${misc:Depends} to Depends.
  * Add lintian overrides.
  * Switch build system to dh, use setup.py.
  * Add libnss-db to Recommends.

 -- Tollef Fog Heen <tfheen@debian.org>  Wed, 18 Mar 2015 14:57:02 +0100

pglistener (3) unstable; urgency=low

  * Use @ instead of . as pubkey separator for gitolite.

 -- Tollef Fog Heen <tfheen@debian.org>  Mon, 18 Mar 2013 13:23:57 +0100

pglistener (2) unstable; urgency=low

  * Add format support to flatfile
  * Add gitolite handler
  * pua updates to handle email field

 -- Tollef Fog Heen <tfheen@debian.org>  Mon, 18 Mar 2013 13:15:39 +0100

pglistener (1) unstable; urgency=low

  * Switch versioning to just be an integer.
  * Fix race condition in nssdb.py where a database would be temporarily
    empty.
  * Freedesktop.org uses slightly different db, fix pua for them.
  * Move libdbix-class-perl to suggests, it's just needed for pua.
  * Make myself the maintainer.
  * Update debian/copyright to be useful.

 -- Tollef Fog Heen <tfheen@debian.org>  Mon, 25 Apr 2011 18:32:03 +0200

pglistener (0-11) lenny; urgency=low

  * Make sure to import psycopg2.extensions too, to make even newer
    versions of psycopg2 happy.

 -- Tollef Fog Heen <tfheen@debian.org>  Wed, 14 Jul 2010 12:10:18 +0200

pglistener (0-10) lenny; urgency=low

  * Get rid of postgresql from Required-Start and Required-Stop in the
    init script.

 -- Tollef Fog Heen <tfheen@debian.org>  Mon, 12 Jul 2010 15:33:37 +0200

pglistener (0-9) lenny; urgency=low

  * Make this compatible with newer versions of psycopg2.

 -- Tollef Fog Heen <tfheen@debian.org>  Mon, 12 Jul 2010 15:21:13 +0200

pglistener (0-8) lenny; urgency=low

  * Add user administration tool.

 -- Tollef Fog Heen <tfheen@debian.org>  Thu, 15 Apr 2010 09:00:22 +0200

pglistener (0-7) lenny; urgency=low

  * Fix typo

 -- Tollef Fog Heen <tfheen@debian.org>  Thu, 25 Feb 2010 08:48:37 +0100

pglistener (0-6) lenny; urgency=low

  * Turn on TCP keepalives if possible.  This should make us better at
    detecting when the connection goes away.

 -- Tollef Fog Heen <tfheen@debian.org>  Tue, 16 Feb 2010 11:36:10 +0100

pglistener (0-5) lenny; urgency=low

  * Fix silly typo in starter.py and update.py where it would fail to
    start since config.read_config has now been renamed to
    config.read_configs.
  * Make starter.py only look at argv[1:] to avoid trying to parse itself
    as a config file.

 -- Tollef Fog Heen <tfheen@debian.org>  Tue, 08 Sep 2009 11:26:18 +0200

pglistener (0-4) lenny; urgency=low

  [ Dafydd Harries ]
  * Add support for config directories in addition to a single file.
  * Add more tests.

  [ Tollef Fog Heen ]
  * Add myself to uploaders.
  * Look for configs in /etc/pglistener/conf.d by default.

 -- Tollef Fog Heen <tfheen@debian.org>  Fri, 28 Aug 2009 23:30:19 +0200

pglistener (0-3) lenny; urgency=low

  * Make sure to include the sshkeys class

 -- Tollef Fog Heen <tfheen@debian.org>  Thu, 21 May 2009 18:56:22 +0200

pglistener (0-2) lenny; urgency=low

  * Add MailingList class

 -- Tollef Fog Heen <tfheen@debian.org>  Thu, 21 May 2009 11:10:15 +0200

pglistener (0-1) unstable; urgency=low

  * Initial package.

 -- Dafydd Harries <daf@debian.org>  Thu, 20 Nov 2008 13:18:22 +0000
